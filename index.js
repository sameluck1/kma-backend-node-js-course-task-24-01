const express = require('express');
const app = express();

const PORT = process.env.PORT || 56201;

app.use(express.text());

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});

app.post('/square', (req, res) => {
  if (isNaN(req.body)) {
    return res.status(400).send("Invalid input. Please provide a valid number.");
  }
  const number = Number(req.body);
  const square = number * number;
  res.json({ number: number, square: square });
});

app.post('/reverse', (req, res) => {
  if (req.headers['content-type'] !== 'text/plain') {
    return res.status(400).send("Content-Type must be text/plain");
  }
  const reversedText = req.body.split('').reverse().join('');
  res.send(reversedText);
});

function isLeapYear(year) {
  year = parseInt(year, 10);
  if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
    return true;
  }
  return false;
}

app.get('/date/:year/:month/:day', (req, res) => {
  const { year, month, day } = req.params;
  const date = new Date(year, month - 1, day);
  if (isNaN(date.getTime())) {
    return res.status(400).json({ error: "Invalid date provided." });
  }
  const weekDay = date.toLocaleDateString('en-US', { weekday: 'long' });
  const leapYearCheck = isLeapYear(year);
  const today = new Date();
  today.setHours(0, 0, 0, 0);
  date.setHours(0, 0, 0, 0);

  const difference = Math.abs(Math.ceil((date - today) / (1000 * 60 * 60 * 24))); //The problem was the lack of math abs

  res.json({
    weekDay: weekDay,
    isLeapYear: leapYearCheck,
    difference: difference
  });
});
